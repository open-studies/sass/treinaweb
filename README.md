# Sass course

This project was made only for study purpose.

To check the preprocessor result on this simples project with the setup I used, use on terminal:

- `nvm`
- your preferred version of node (to the most recent, run: `nvm use node`)
- npm global package for sass, explained on [sass-website](https://sass-lang.com/install) (cli > 'install anywhere section). Spoiler: `npm install -g sass`
- `watch` command when the project is opened: `sass --watch src/sass/:dist/css/`
- `npm i` (but there isn't yet dependencies)
